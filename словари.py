Python 3.9.1 (tags/v3.9.1:1e5d33e, Dec  7 2020, 17:08:21) [MSC v.1927 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> d = dict()
>>> 
>>> type(d)
<class 'dict'>
>>> d1 = {}
>>> type(d1)
<class 'dict'>
>>> d = dict(Ivan="director",Mark="assistant")
>>> d
{'Ivan': 'director', 'Mark': 'assistant'}
>>> d1 = {"A1":"123", "A2":"456"}
>>> d1
{'A1': '123', 'A2': '456'}
>>> d = {"Ru":"Saints-Peterburg","USA":"NY"}
>>> d
{'Ru': 'Saints-Peterburg', 'USA': 'NY'}
>>> d["Chona"]="Shanghai"
>>> d
{'Ru': 'Saints-Peterburg', 'USA': 'NY', 'Chona': 'Shanghai'}
>>> d1
{'A1': '123', 'A2': '456'}
>>> del d1["A1"]
>>> d1
{'A2': '456'}
>>> "A1" in d1
False
>>> "A2" in d1
True
>>> d
{'Ru': 'Saints-Peterburg', 'USA': 'NY', 'Chona': 'Shanghai'}
>>> d1
{'A2': '456'}
>>> d1.clear
<built-in method clear of dict object at 0x000001A868E4A340>
d
>>> 
>>> d
{'Ru': 'Saints-Peterburg', 'USA': 'NY', 'Chona': 'Shanghai'}
>>> d1
{'A2': '456'}
>>> d.clear
<built-in method clear of dict object at 0x000001A868E4A2C0>
>>> d.clear()
>>> d
{}
>>> d1
{'A2': '456'}
>>> d2 = d1.copy()
>>> d2
{'A2': '456'}
>>> d2["A2"]="123"
>>> d2
{'A2': '123'}
>>> d1
{'A2': '456'}
>>> d2
{'A2': '123'}
>>> d2=d1.copy()
>>> d2
{'A2': '456'}
>>> d.get("A2")
>>> d
{}
>>> d2.get("A2")
'456'
>>> d
{}
>>> d2.keys()
dict_keys(['A2'])
>>> d2
{'A2': '456'}
>>> d2.values()
dict_values(['456'])
>>> d
{}
>>> d2
{'A2': '456'}
>>> d2.update({"A1":"321"})
>>> d2
{'A2': '456', 'A1': '321'}
>>> d2
{'A2': '456', 'A1': '321'}
>>> d.pop("A2")'321'
SyntaxError: invalid syntax
>>> d2.pop("A2")'321'
SyntaxError: invalid syntax
>>> d2.pop("A2")"321"
SyntaxError: invalid syntax
>>> d2
{'A2': '456', 'A1': '321'}
>>> d2.get("A1")
'321'
>>> 
= RESTART: C:/Users/yaro/AppData/Local/Programs/Python/Python39/english-russian dictionary.py
Traceback (most recent call last):
  File "C:/Users/yaro/AppData/Local/Programs/Python/Python39/english-russian dictionary.py", line 1, in <module>
    Dictionary = dict("Assistant"-"ассистент")
TypeError: unsupported operand type(s) for -: 'str' and 'str'
>>> 
= RESTART: C:/Users/yaro/AppData/Local/Programs/Python/Python39/english-russian dictionary.py
Traceback (most recent call last):
  File "C:/Users/yaro/AppData/Local/Programs/Python/Python39/english-russian dictionary.py", line 1, in <module>
    D = dict("Assistant"-"ассистент")
TypeError: unsupported operand type(s) for -: 'str' and 'str'
>>> 