Python 3.9.1 (tags/v3.9.1:1e5d33e, Dec  7 2020, 17:08:21) [MSC v.1927 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> lst = [1,2,3,4,5]
>>> print(type(lst))
<class 'list'>
>>> tpl = tuple(lst)
>>> print(tpl)
(1, 2, 3, 4, 5)
>>> a = (1,2,3)
>>> print(type(a))
<class 'tuple'>
>>> tuple(range(10,20,30))
(10,)
>>> tuple(range(20))
(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
>>> print(a)
(1, 2, 3)
>>> del(a)
>>> print(a)
Traceback (most recent call last):
  File "<pyshell#10>", line 1, in <module>
    print(a)
NameError: name 'a' is not defined
>>> tpl = [2,3,4,5,6,7,8]
>>> print(type(tpl))
<class 'list'>
>>> lst = list(tpl)
>>> print(type(lst))
<class 'list'>
>>> tpl = (2,3,4,5,6,7,8)
>>> print(type(tpl))
<class 'tuple'>
>>> lst = list(tpl)
>>> print(type(lst))
<class 'list'>
>>> a,a1=('smth', 10)
>>> a
'smth'
>>> a1
10
>>> a,a1=a1,a
>>> a
10
>>> a1
'smth'
>>> c = set({1,2,3,4,5,6,1,23})
>>> c
{1, 2, 3, 4, 5, 6, 23}
>>> g = set('Python')
>>> g
{'y', 'h', 't', 'n', 'P', 'o'}
>>> type(g)
<class 'set'>
>>> sc = c | g
>>> sc
{1, 2, 3, 4, 5, 6, 'y', 'h', 't', 'n', 23, 'P', 'o'}
>>> sc = c & g
>>> sc
set()
>>> j = sc 6 s
SyntaxError: invalid syntax
>>> j = sc ^ s
Traceback (most recent call last):
  File "<pyshell#36>", line 1, in <module>
    j = sc ^ s
NameError: name 's' is not defined
>>> sc = s ^ c
Traceback (most recent call last):
  File "<pyshell#37>", line 1, in <module>
    sc = s ^ c
NameError: name 's' is not defined
>>> s
Traceback (most recent call last):
  File "<pyshell#38>", line 1, in <module>
    s
NameError: name 's' is not defined
>>> j = c ^ g
>>> j
{1, 2, 3, 4, 5, 6, 't', 23, 'y', 'h', 'n', 'P', 'o'}
>>> sc.add("Py")
>>> sc
{'Py'}
>>> sc = c*g
Traceback (most recent call last):
  File "<pyshell#43>", line 1, in <module>
    sc = c*g
TypeError: unsupported operand type(s) for *: 'set' and 'set'
>>> sc = c * g
Traceback (most recent call last):
  File "<pyshell#44>", line 1, in <module>
    sc = c * g
TypeError: unsupported operand type(s) for *: 'set' and 'set'
>>> c
{1, 2, 3, 4, 5, 6, 23}
>>> g
{'y', 'h', 't', 'n', 'P', 'o'}
>>> sc = c | g
>>> sc
{1, 2, 3, 4, 5, 6, 'y', 'h', 't', 'n', 23, 'P', 'o'}
>>> sc.add('Py')
>>> sc
{1, 2, 3, 4, 5, 6, 'y', 'h', 't', 'n', 23, 'P', 'o', 'Py'}
>>> sc.updatte([,15,16,17])
SyntaxError: invalid syntax
>>> sc.update({15,16,17})'
SyntaxError: EOL while scanning string literal
>>> sc.update({15,16,17})
>>> sc
{1, 2, 3, 4, 5, 6, 'y', 15, 'h', 16, 't', 17, 'n', 23, 'P', 'o', 'Py'}
>>> sc.remove(3)
>>> sc
{1, 2, 4, 5, 6, 'y', 15, 'h', 16, 't', 17, 'n', 23, 'P', 'o', 'Py'}
>>> sc.remove("y")
>>> sc
{1, 2, 4, 5, 6, 15, 'h', 16, 't', 17, 'n', 23, 'P', 'o', 'Py'}
>>> 