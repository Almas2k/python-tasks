# дано: s = 'ab12c59p7dq'
# надо: извлечь цифры в список digits,
s = 'ab12c59p7dq'
digits = []
for symbol in s:
    if '1234567890'.find(symbol) != -1:
        digits.append(int(symbol))
print('Вывод списка: ')
print(digits)
a = ['red', 'green', 'blue']
print(' '.join(a))
# вернёт red green blue
print(''.join(a))
# вернёт redgreenblue
print('***'.join(a))
# вернёт red***green***blue
A = [1, 2, 3, 4, 5, 6,  7]
A[::-2] = [10, 20, 30, 40]
print('Вывод списка:', A)
Rainbow = ['Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Indigo', 'Violet']
print('Вывод первого эелмента списка в его первоначальном виде:',Rainbow[0])
Rainbow[0] = 'красный' #Обозначаем элемент 
print('Выведем радугу:')
for i in range(len(Rainbow)):
    print(Rainbow[i])
a = [1, 2, 3]
b = [4, 5]
c = a + b
d = b * 3
print('Вывод списка: ')
print([7, 8] + [9])
print([0, 1] * 3)


